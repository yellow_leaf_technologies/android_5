import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';

class RaisedAction extends StatelessWidget {
  final VoidCallback action;
  final String image;

  const RaisedAction(this.image,  this.action, {Key key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width;
    if (MediaQuery.of(context).size.width > 420) {
      width = 176.0;
    } else if (MediaQuery.of(context).size.width > 400) {
      width = 146.0;
    } else {
      width = 126.0;
    }
    return RaisedButton(
      onPressed: action,
      splashColor: AppColors.RIPPLE,
      elevation: 0,
      shape: CircleBorder(),
      padding: EdgeInsets.all(0.0),
      child: Image.asset(image, width: width),
    );
  }
}
