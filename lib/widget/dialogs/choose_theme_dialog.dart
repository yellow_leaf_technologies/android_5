import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/dashboard_screen.dart';
import 'package:wevpn/themes/custom_theme.dart';
import 'package:wevpn/themes/themes.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/text_field.dart';

class ChooseThemeDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChooseThemeDialog();
  }
}

class _ChooseThemeDialog extends State<ChooseThemeDialog> {
  final controller = StreamController<List<Theme>>();
  int _groupValue = -1;
  int selected = 0;


  void _changeTheme(BuildContext buildContext, ThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
        backgroundColor: AppColors.TRANSPARENT,
        child: AnimatedContainer(
            duration: Duration(milliseconds: 500),
            child: Scaffold(
              backgroundColor: AppColors.TRANSPARENT,
              body: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(AppSizes.DIALOG_RADIUS),
                    ),
                    color: prefix0.Theme.of(context).backgroundColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Column(children: <Widget>[
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
                          child: Align(
                            child: Image.asset(
                              "assets/images/ic_bucket.png",
                              width: 46,
                            ),
                          ),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 18, 0, 20),
                            child: CustomText(
                              "Choose your theme",
                              fontSize: 20.0,
                              textColor: AppColors.PINK,
                            )),
                      ]),
                      Expanded(
                          child: FutureBuilder<List<Theme>>(
                        future: getThemes(),
                        initialData: List(),
                        builder: (BuildContext context,
                            AsyncSnapshot<List<Theme>> data) {
                          List<Theme> list = data.data;
                          if (list.length == 0) {
                            return Container();
                          }
                          return ListView.builder(
                              itemCount: list.length,
                              itemBuilder: (context, index) {
                                return ExpansionTile(
                                  onExpansionChanged: (value) {
                                    selected = index;
                                    _groupValue = list[index].getId();
                                    _changeTheme(
                                        context, list[index].getThemeKey());
                                  },
                                  title: _buildParentExpandableContent(
                                      list[index]),
                                  children:
                                      _buildExpandableContent(list[index]),
                                );
                              });
                        },
                      )),
                      ButtonFullAction(
                        "Apply & Continue",
                        () async {
                          _openDashboardScreen();
                        },
                        marginLeft: 43,
                        marginRight: 43,
                        marginBottom: 43,
                        height: AppSizes.CUSTOM_BUTTON_SIZE,
                      ),
                    ],
                  )),
            )));
  }

  Future<List<Theme>> getThemes() async {
    List<Theme> themeList = new List();
    themeList.add(
        Theme(0, "Light Theme", "assets/images/ic_theme.png", ThemeKeys.LIGHT));
    themeList.add(Theme(
        1, "Dark Theme", "assets/images/ic_theme_dark.png", ThemeKeys.DARK));
    return themeList;
  }

  _buildParentExpandableContent(Theme theme) {
    return prefix0.Theme(
        data: ThemeData(
            unselectedWidgetColor: AppColors.CHECK_BOX_UNCHECK,
            accentColor: AppColors.PINK),
        child: RadioListTile(
          title: CustomText(
            theme.getTitle(),
            fontWeight: FontWeight.w600,
            textColor: prefix0.Theme.of(context).accentColor,
          ),
          value: theme.getId(),
          groupValue: _groupValue,
        ));
  }

  _buildExpandableContent(Theme theme) {
    List<Widget> columnContent = [];
    columnContent.add(
      new ListTile(
          title: Padding(
        padding: EdgeInsets.all(0),
        child: Image.asset(
          theme.getImagePath(),
          height: MediaQuery.of(context).size.height / 2.8,
        ),
      )),
    );

    return columnContent;
  }

  void _openDashboardScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => DashboardScreen()),
    );
  }
}

class Theme {
  final String title;
  final String imagePah;
  final int id;
  final ThemeKeys key;

  Theme(this.id, this.title, this.imagePah, this.key);

  getId() {
    return id;
  }

  getTitle() {
    return title;
  }

  getImagePath() {
    return imagePah;
  }

  getThemeKey() {
    return key;
  }
}
