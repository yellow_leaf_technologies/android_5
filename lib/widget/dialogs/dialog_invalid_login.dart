import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/widget/button_action.dart';
import 'package:wevpn/widget/text_field.dart';

class InvalidateLoginDialog extends StatefulWidget {
  final VoidCallback yesAction, noAction;

  const InvalidateLoginDialog({
    Key key,
    @required this.yesAction,
    @required this.noAction,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _InvalidateDialog();
  }
}

class _InvalidateDialog extends State<InvalidateLoginDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      elevation: 0,
      backgroundColor: AppColors.TRANSPARENT,
      child: Container(
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(AppSizes.DIALOG_RADIUS),
            ),
            color: Theme.of(context).backgroundColor,
          ),
          child: Padding(
              padding: EdgeInsets.fromLTRB(0, 60, 0, 63),
              child: Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                Align(
                  alignment: Alignment.center,
                  child: Container(
                    child: Image.asset(
                      "assets/images/invalid_login.png",
                      width: 56,
                      height: 52,
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                    child: Text(
                      "Invalid Login",
                      style: TextStyle(
                          fontSize: 20.0,
                          color: AppColors.PINK,
                          fontFamily: "Averta",
                          fontWeight: FontWeight.w600),
                    )),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 24),
                    child: Text(
                      "The email or password you\nentered is incorrect. Try again or\nreset your password",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 14.0,
                          height: 2.0,
                          color: Theme.of(context).accentColor,
                          fontFamily: "Averta",
                          fontWeight: FontWeight.w400),
                    )),
                ButtonAction(
                  "Try again",
                  widget.yesAction,
                  paddingLeft: 46,
                  paddingRight: 46,
                  height: AppSizes.CUSTOM_BUTTON_SIZE,
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 11, 0, 0),
                    child: CustomText(
                      "Or Reset password",
                      isUnderline: true,
                      textColor: Theme.of(context).unselectedWidgetColor,
                    ))
              ])),
        ),
      ),
    );
  }
}
