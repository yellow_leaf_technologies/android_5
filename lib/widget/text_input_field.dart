import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_styles.dart';

class TextInputField extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final FocusNode focusNode;
  final TextCapitalization textCapitalization;

  TextInputField(this.controller, this.hint, this.focusNode,
      {Key key, this.textCapitalization = TextCapitalization.none})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextField(
      maxLines: 1,
      controller: controller,
      textCapitalization: textCapitalization,
      decoration: InputDecoration(
        border: InputBorder.none,
        hintText: hint,
        contentPadding: EdgeInsets.all(16.0),
        filled: true,
        hintStyle: TextStyle(
            color: Theme.of(context).cardColor,
            fontSize: 14,
            fontFamily: "Averta",
            fontWeight: FontWeight.w400),
        fillColor: Theme.of(context).hintColor,
      ),
    );
  }
}
