import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:wevpn/widget/text_field.dart';
import 'package:wevpn/widget/text_input_field.dart';

class LoginForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginForm();
}

class _LoginForm extends State<LoginForm> {
  final TextEditingController controllerUsername = new TextEditingController();
  final TextEditingController controllerPassword = new TextEditingController();
  final FocusNode focusNode = new FocusNode();

  bool passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Padding(
          padding: EdgeInsets.fromLTRB(40, 17, 40, 0),
          child: Row(
            children: <Widget>[
              CustomText("Username/Email",
                  textColor: prefix0.Theme.of(context).accentColor)
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(40, 16, 40, 0),
            child: TextInputField(
                controllerUsername, "Enter username/email here...", focusNode)),
        Padding(
          padding: EdgeInsets.fromLTRB(40, 17, 40, 0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              CustomText(
                "Passsword",
                textColor: prefix0.Theme.of(context).accentColor,
              ),
              CustomText("Forgot Password?",
                  textColor: prefix0.Theme.of(context).unselectedWidgetColor,
                  fontSize: 13.0,
                  isUnderline: true),
            ],
          ),
        ),
        Padding(
            padding: EdgeInsets.fromLTRB(40, 15, 40, 0),
            child: TextInputField(
              controllerPassword,
              "Enter password here...",
              focusNode,
            )),
      ],
    );
  }
//    Padding(
//            padding: EdgeInsets.fromLTRB(40, 15, 40, 0),
//            child: prefix0.TextFormField(
//              controller: controllerPassword,
//              obscureText: passwordVisible,
//              decoration: prefix0.InputDecoration(
//                hintText: "Enter password here...",
//                suffixIcon: prefix0.IconButton(
//                  icon: Icon(
//                    // Based on passwordVisible state choose the icon
//                    passwordVisible
//                        ? prefix0.Icons.visibility
//                        : prefix0.Icons.visibility_off,
//                    color: prefix0.Theme.of(context).primaryColorDark,
//                  ),
//                  onPressed: () {
//                    passwordVisible = !passwordVisible;
//                  },
//                ),
//              ),
//            )),
}
