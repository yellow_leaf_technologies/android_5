import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';

class CustomText extends StatelessWidget {
  final String text;
  final double paddingLeft;
  final double paddingTop;
  final double paddingAll;
  final Color textColor;
  final double fontSize;
  final FontWeight fontWeight;
  final bool isUnderline;
  final TextStyle textStyle;

  const CustomText(this.text,
      {Key key,
      this.paddingLeft = 0.0,
      this.paddingTop = 0.0,
      this.paddingAll,
      this.textColor,
      this.fontWeight = FontWeight.w400,
      this.fontSize = 14.0,
      this.isUnderline = false,
      this.textStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: paddingAll == null
          ? EdgeInsets.fromLTRB(paddingLeft, paddingTop, 0, 0)
          : EdgeInsets.all(paddingAll),
      child: Text(
        text,
        style: textStyle == null
            ? TextStyle(
                decoration: isUnderline ? TextDecoration.underline : null,
                fontSize: fontSize,
                fontFamily: 'Averta',
                fontWeight: fontWeight,
                color: textColor == null ? AppColors.TEXT : textColor)
            : textStyle,
      ),
    );
  }
}
