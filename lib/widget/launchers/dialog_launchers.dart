import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';

class DialogLaunchers {
  static Future showDialog(
      {@required BuildContext context, @required Widget dialog}) {
    return showGeneralDialog(
        context: context,
        barrierColor: AppColors.DIALOG_FADER,
        barrierDismissible: true,
        // should dialog be dismissed when tapped outside
        barrierLabel:
            MaterialLocalizations.of(context).modalBarrierDismissLabel,
        transitionDuration: Duration(milliseconds: 400),
        // how long it takes to popup dialog after button click
        pageBuilder: (_, __, ___) {
          return dialog;
        });
  }
}
