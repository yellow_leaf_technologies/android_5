import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';

class ButtonFullAction extends StatelessWidget {
  final VoidCallback action;
  final String text;
  final double marginTop;
  final double marginLeft;
  final double marginRight;
  final double height;
  final double marginBottom;

  const ButtonFullAction(this.text, this.action,
      {Key key,
        this.marginTop = 0.0,
        this.marginBottom = 0.0,
        this.marginLeft = 0.0,
        this.marginRight = 0.0,
        this.height = AppSizes.BUTTON_SIZE})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.fromLTRB(marginLeft, marginTop, marginRight, marginBottom),
      height: height,
      child: RaisedButton(
        child: Text(
          text,
          style: TextStyle(
              color: AppColors.BUTTON_TEXT_COLOR,
              fontSize: 16,
              fontWeight: FontWeight.w600),
        ),
        onPressed: action,
      ),
    );
  }
}
