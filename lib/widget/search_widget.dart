import 'package:flutter/material.dart';

class SearchWidget extends StatefulWidget {
  final ValueChanged<String> onSearch;
  final ValueChanged<bool> onFocus;
  final VoidCallback onSearchCancel, onSearchStarted;

  const SearchWidget(
    this.onSearch, {
    Key key,
    this.onFocus,
    this.onSearchStarted,
    this.onSearchCancel,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _SearchWidgetState();
  }
}

class _SearchWidgetState extends State<SearchWidget> {
  final FocusNode focusNode = new FocusNode();
  final TextEditingController controller = new TextEditingController();
  bool _hasText = false;

  @override
  void initState() {
    focusNode.addListener(_onFocus);
    controller.addListener(_onTextEntered);
    super.initState();
  }

  @override
  void dispose() {
    focusNode.removeListener(_onFocus);
    focusNode.dispose();
    controller.removeListener(_onTextEntered);
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 24.0, horizontal: 40.0),
      child: Container(
        padding: EdgeInsets.fromLTRB(16, 2, 6, 0),
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: Row(
          children: <Widget>[
            Expanded(
              child: TextField(
                decoration: new InputDecoration(
                  hintText: "Search for a country or city",
                  hintStyle: TextStyle(
                      color: Theme.of(context).cardColor,
                      fontFamily: "Averta",
                      fontSize: 13.0),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                ),
                focusNode: focusNode,
                controller: controller,
                style: new TextStyle(
                    color: Theme.of(context).accentColor,
                    fontFamily: "Averta",
                    fontSize: 13.0),
                maxLines: 1,
                onChanged: widget.onSearch,
                keyboardType: TextInputType.text,
                textAlignVertical: TextAlignVertical.center,
              ),
            ),
            _hasText
                ? IconButton(
                    onPressed: () {
                      controller.text = "";
                      widget.onSearch("");
                      focusNode.unfocus();
                      setState(() {
                        _hasText = false;
                      });
                    },
                    icon:
                        Icon(Icons.clear, color: Theme.of(context).accentColor),
                  )
                : IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.search,
                        color: Theme.of(context).accentColor),
                  )
          ],
        ),
      ),
    );
  }

  void _onFocus() {
    if (widget.onFocus != null) widget.onFocus(focusNode.hasFocus);
    if (widget.onSearchCancel != null &&
        !focusNode.hasFocus &&
        controller.text.isEmpty) {
      widget.onSearchCancel();
    }
  }

  void _onTextEntered() {
    if (!_hasText && controller.text.isNotEmpty) {
      if (widget.onSearchStarted != null) widget.onSearchStarted();
      setState(() {
        _hasText = true;
      });
    }
    if (_hasText & controller.text.isEmpty) {
      setState(() {
        _hasText = false;
      });
    }
  }
}
