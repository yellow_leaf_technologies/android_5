import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';

class ButtonAction extends StatelessWidget {
  final VoidCallback action;
  final String text;
  final double paddingLeft;
  final double paddingRight;
  final double height;

  const ButtonAction(this.text, this.action,
      {Key key,
      this.paddingLeft = 0.0,
      this.paddingRight = 0.0,
      this.height = AppSizes.BUTTON_SIZE})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(

      height: height,
      child: RaisedButton(
          color: AppColors.PINK,
        padding: EdgeInsets.fromLTRB(paddingLeft, 0, paddingRight, 0),
        child: Text(
          text,
          style: TextStyle(
              color: AppColors.BUTTON_TEXT_COLOR,
              fontSize: 16,
              fontWeight: FontWeight.w600),
        ),
        onPressed: action,
      ),
    );
  }
}
