import 'package:flutter/material.dart';

class AppColors {
//  all
  static const PINK = Color(0xFFFD3E72);
  static const RIPPLE = Color(0xFF5B5863);
  static const GREEN_WHITE = Color(0xFF62D5B4);
  static const GREEN = Color(0xFF50BF9F);
  static const BUTTON_TEXT_COLOR = Color(0xFFFCF9F2);
  static const DIALOG_FADER = Color(0xAA5E5B55);

  static const TEXT = Color(0xFF5B5863);
  static const WHITE = Color(0xFFFFFFFF);

  static const TRANSPARENT = Color(0x00000000);
  static const CHECK_BOX_UNCHECK = Color(0xFFD4D2CC);
  static const TEXT_DESCRIPTION = Color(0xFF969599);
  static const LINE_COLOR = Color(0xFFE8E6E0);
  static const LINE_SUB_COLOR = Color(0xFFEDEBE5);

//  Light theme
  static const BACKGROUND = Color(0xFFF9F8F4);
  static const BACKGROUND_BASE = Color(0xFFF5F3ED);
  static const BACKGROUND_APP_BAR_TITLE = Color(0xFF5B5863);
  static const TEXT_INPUT_HINT = Color(0xFF807E85);
  static const BACKGROUND_TEXT_FIELD = Color(0xFFFFFFFF);
  static const UNDERLINE_TEXT = Color(0xFF8F8D91);
  static const SETTING_BLOCK_TITLE = Color(0xFFAFADB2);
  static const SETTING_DESCRIPTIONS = Color(0xFF969599);
  static const BACKGROUND_LINE = Color(0xFFEDEBE5);

//  Dart theme
  static const BACKGROUND_DART = Color(0xFF3F3D45);
  static const BACKGROUND_BASE_DART = Color(0xFF36343B);
  static const BACKGROUND_APP_BAR_TITLE_DART = Color(0xFFC7C5C4);
  static const TEXT_INPUT_HINT_DART = Color(0xFF807E85);
  static const BACKGROUND_TEXT_FIELD_DART = Color(0xFF46444D);
  static const UNDERLINE_TEXT_DART = Color(0xFFACA9B0);
  static const SETTING_BLOCK_TITLE_DART = Color(0xFF626064);
  static const SETTING_DESCRIPTIONS_DART = Color(0xFF7E7C80);
  static const BACKGROUND_TOAST_DART = Color(0xFF333138);
  static const BACKGROUND_LINE_DART = Color(0xFF312F36);
}
