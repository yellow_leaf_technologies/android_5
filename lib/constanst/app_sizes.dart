
class AppSizes {
  static const BORDER_RADIUS = 4.0;
  static const BUTTON_SIZE = 40.0;
  static const CUSTOM_BUTTON_SIZE = 48.0;
  static const DIALOG_RADIUS = 20.0;
  static const LOCATION_RADIUS = 5.0;
}
