import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:wevpn/bloc/preferences_provider.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/main.dart';
import 'package:wevpn/themes/custom_theme.dart';
import 'package:wevpn/themes/themes.dart';
import 'package:wevpn/widget/button_action.dart';
import 'package:wevpn/widget/text_field.dart';

class SettingsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Settings();
  }
}

class _Settings extends State<SettingsScreen> {
  bool isLaunch = false;
  bool isConnect = false;
  bool isKillVPN = true;
  bool isAutoConnect = false;
  bool isAutoUpdate = false;
  int _groupValue = MyApp.isLight ? 0 : 1;
  int _groupProtocolValue = -1;

  void _changeTheme(BuildContext buildContext, ThemeKeys key) {
    CustomTheme.instanceOf(buildContext).changeTheme(key);
    PreferencesProvider().saveTheme(key == ThemeKeys.LIGHT);
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
        duration: Duration(milliseconds: 500),
        child: MaterialApp(
            home: Scaffold(
                backgroundColor: Theme.of(context).backgroundColor,
                appBar: PreferredSize(
                  preferredSize: Size.fromHeight(81),
                  child: Column(
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.fromLTRB(20, 41, 0, 0),
                          child: Stack(children: <Widget>[
                            new IconButton(
                              padding: EdgeInsets.all(5),
                              icon: Image.asset(
                                "assets/images/ic_back.png",
                                width: 28.0,
                              ),
                              onPressed: () => Navigator.of(context).pop(null),
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
                                child: Center(
                                    child: Text(
                                  "Settings",
                                  style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: 18.0,
                                      fontFamily: "Averta",
                                      fontWeight: FontWeight.w600),
                                )))
                          ]))
                    ],
                  ),
                ),
                body: Scaffold(
                    backgroundColor: Theme.of(context).hintColor,
                    body: SingleChildScrollView(
                        child: Column(children: <Widget>[
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              color: Theme.of(context).backgroundColor,
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 20, 0, 20),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          "assets/images/ic_settings.png",
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: CustomText(
                                            "GENERAL",
                                            textStyle: initBlockStyle(),
                                          ))
                                    ],
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 20, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor:
                                        AppColors.CHECK_BOX_UNCHECK,
                                    selectedRowColor: AppColors.PINK,
                                  ),
                                  child: CheckboxListTile(
                                    title: CustomText(
                                      "Launch on system startup",
                                      textStyle: itemCheckTextStyle(),
                                    ),
                                    value: isLaunch,
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    checkColor: AppColors.WHITE,
                                    activeColor: AppColors.PINK,
                                    onChanged: (value) {
                                      setState(() {
                                        isLaunch = value;
                                      });
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      selectedRowColor: AppColors.PINK,
                                      inputDecorationTheme:
                                          InputDecorationTheme()),
                                  child: CheckboxListTile(
                                    title: CustomText("Connect on launch",
                                        textStyle: itemCheckTextStyle()),
                                    value: isConnect,
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    checkColor: AppColors.WHITE,
                                    activeColor: AppColors.PINK,
                                    onChanged: (value) {
                                      setState(() {
                                        isConnect = value;
                                      });
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(40, 8, 0, 0),
                              child: CustomText(
                                "APPEARANCE",
                                fontSize: 12.0,
                                textColor: Theme.of(context).cardColor,
                              )),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      accentColor: AppColors.PINK),
                                  child: RadioListTile(
                                    title: CustomText("Light Theme",
                                        textStyle: itemTextStyle()),
                                    groupValue: _groupValue,
                                    value: 0,
                                    onChanged: (value) {
                                      setState(() {
                                        this._groupValue = value;
                                      });
                                      _changeTheme(context, ThemeKeys.LIGHT);
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 20),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      accentColor: AppColors.PINK),
                                  child: RadioListTile(
                                    title: CustomText("Dark Theme",
                                        textStyle: itemTextStyle()),
                                    groupValue: _groupValue,
                                    value: 1,
                                    onChanged: (value) {
                                      setState(() {
                                        this._groupValue = value;
                                      });
                                      _changeTheme(context, ThemeKeys.DARK);
                                    },
                                  ))),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              color: Theme.of(context).backgroundColor,
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 20, 0, 22),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          "assets/images/ic_protocol.png",
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: CustomText("PROTOCOL",
                                              textStyle: initBlockStyle()))
                                    ],
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 20, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      accentColor: AppColors.PINK),
                                  child: RadioListTile(
                                    groupValue: _groupProtocolValue,
                                    title: CustomText("Automatic (Recommended)",
                                        textStyle: itemTextStyle()),
                                    value: 0,
                                    onChanged: (value) {
                                      setState(() {
                                        _groupProtocolValue = value;
                                      });
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(86, 0, 0, 0),
                              child: CustomText(
                                  "WeVPN will automatically\nselect the protocol most\nsuitable for you.",
                                  textStyle: initDescriptionStyle())),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      accentColor: AppColors.PINK),
                                  child: RadioListTile(
                                    title: CustomText("UDP - OpenVPN",
                                        textStyle: itemTextStyle()),
                                    value: 1,
                                    groupValue: _groupProtocolValue,
                                    onChanged: (value) {
                                      setState(() {
                                        _groupProtocolValue = value;
                                      });
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(86, 0, 0, 0),
                              child: CustomText(
                                  "Best combination od security\nand speed, but may not work\non the all networks",
                                  textStyle: initDescriptionStyle())),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                      unselectedWidgetColor:
                                          AppColors.CHECK_BOX_UNCHECK,
                                      accentColor: AppColors.PINK),
                                  child: RadioListTile(
                                    title: CustomText("TCP - OpenVPN",
                                        textStyle: itemTextStyle()),
                                    value: 2,
                                    groupValue: _groupProtocolValue,
                                    onChanged: (value) {
                                      setState(() {
                                        _groupProtocolValue = value;
                                      });
                                    },
                                  ))),
                          Padding(
                            padding: EdgeInsets.fromLTRB(86, 0, 0, 0),
                            child: CustomText(
                                "Likely to function on all types\nof networks, but might be\nslower than UDP.",
                                textStyle: initDescriptionStyle()),
                          )
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              margin: EdgeInsets.fromLTRB(0, 40, 0, 0),
                              color: Theme.of(context).backgroundColor,
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 22, 0, 22),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          "assets/images/ic_privacy.png",
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: CustomText("PRIVACY",
                                              textStyle: initBlockStyle()))
                                    ],
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(24, 20, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor:
                                        AppColors.CHECK_BOX_UNCHECK,
                                    selectedRowColor: AppColors.PINK,
                                  ),
                                  child: SwitchListTile(
                                    title: CustomText("VPN Kill (On)",
                                        textStyle: itemTextStyle()),
                                    value: isKillVPN,
                                    inactiveTrackColor:
                                        AppColors.CHECK_BOX_UNCHECK,
                                    activeColor: AppColors.PINK,
                                    onChanged: (value) {
                                      setState(() {
                                        isKillVPN = value;
                                      });
                                    },
                                  ))),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 0, 0, 0),
                            child: CustomText(
                                "Prevent leaks by blocking\ntraffic outside the VPN",
                                textStyle: initDescriptionStyle()),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor:
                                        AppColors.CHECK_BOX_UNCHECK,
                                    selectedRowColor: AppColors.PINK,
                                  ),
                                  child: CheckboxListTile(
                                    title: CustomText("Auto-Reconnect",
                                        textStyle: itemTextStyle()),
                                    value: isAutoConnect,
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    checkColor: AppColors.WHITE,
                                    activeColor: AppColors.PINK,
                                    onChanged: (value) {
                                      setState(() {
                                        isAutoConnect = value;
                                      });
                                    },
                                  ))),
                          Padding(
                            padding: EdgeInsets.fromLTRB(86, 0, 0, 40),
                            child: CustomText(
                                "In the event the VPN\nconnection is lost, WeVPN will\nautomatically reconnect.",
                                textStyle: initDescriptionStyle()),
                          )
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              color: Theme.of(context).backgroundColor,
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 22, 0, 22),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                          "assets/images/ic_account.png",
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: CustomText("ACCOUNT",
                                              textStyle: initBlockStyle()))
                                    ],
                                  ))),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 40, 0, 0),
                            child: CustomText(
                              "Registered Email:",
                              fontSize: 12.0,
                              textColor: Theme.of(context).cardColor,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 11, 0, 0),
                            child: CustomText(
                              "hi@eslamelshereef.com",
                              textColor: Theme.of(context).accentColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 24, 0, 0),
                            child: CustomText(
                              "STATUS:",
                              fontSize: 12.0,
                              textColor: Theme.of(context).cardColor,
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(40, 11, 0, 0),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset(
                                    "assets/images/ic_good_green.png",
                                    width: 12,
                                  ),
                                  CustomText(
                                    " Active",
                                    textColor: AppColors.GREEN_WHITE,
                                    fontWeight: FontWeight.w600,
                                  ),
                                  CustomText(
                                    " (One-Month Plan)",
                                    textColor: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.w600,
                                  )
                                ],
                              )),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 24, 0, 0),
                            child: CustomText(
                              "RENESW:",
                              fontSize: 12.0,
                              textColor: Theme.of(context).cardColor,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 11, 0, 0),
                            child: CustomText(
                              "November 1, 2019 (24 days left)",
                              fontSize: 14.0,
                              textColor: Theme.of(context).accentColor,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 24, 0, 40),
                            child: CustomText(
                              "Sign out",
                              fontSize: 13.0,
                              textColor:
                                  Theme.of(context).unselectedWidgetColor,
                              isUnderline: true,
                            ),
                          ),
                        ],
                      ),
                      Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                              color: Theme.of(context).backgroundColor,
                              child: Padding(
                                  padding: EdgeInsets.fromLTRB(40, 22, 0, 22),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset("assets/images/ic_help.png",
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              24),
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(8, 0, 0, 0),
                                          child: CustomText("HELP",
                                              textStyle: initBlockStyle()))
                                    ],
                                  ))),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 40, 0, 0),
                            child: CustomText(
                              "VERSION:",
                              fontSize: 12.0,
                              textColor: Theme.of(context).cardColor,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(40, 11, 0, 0),
                            child: CustomText(
                              "v3.5.1 (Build 03548)",
                              fontSize: 14.0,
                              textColor: Theme.of(context).accentColor,
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.fromLTRB(14, 0, 0, 0),
                              child: Theme(
                                  data: ThemeData(
                                    unselectedWidgetColor:
                                        AppColors.CHECK_BOX_UNCHECK,
                                    selectedRowColor: AppColors.PINK,
                                  ),
                                  child: CheckboxListTile(
                                    title: CustomText(
                                      "Enable Auto-update",
                                      textStyle: itemCheckTextStyle(),
                                    ),
                                    value: isAutoUpdate,
                                    controlAffinity:
                                        ListTileControlAffinity.leading,
                                    checkColor: AppColors.WHITE,
                                    activeColor: AppColors.PINK,
                                    onChanged: (value) {
                                      setState(() {
                                        isAutoUpdate = value;
                                      });
                                    },
                                  ))),
                          Padding(
                              padding: EdgeInsets.fromLTRB(40, 16, 0, 40),
                              child: ButtonAction(
                                "Contact Us!",
                                () {},
                                paddingLeft: 34,
                                paddingRight: 34,
                                height: AppSizes.CUSTOM_BUTTON_SIZE,
                              )),
                        ],
                      )
                    ]))))));
  }

  TextStyle initBlockStyle() {
    return TextStyle(
      fontSize: 12,
      fontFamily: "Averta",
      color: Theme.of(context).cardColor,
      fontWeight: FontWeight.w600,
    );
  }

  TextStyle itemTextStyle() {
    return TextStyle(
        fontSize: 13.0,
        fontFamily: "Averta",
        color: Theme.of(context).accentColor,
        fontWeight: FontWeight.w600);
  }

  TextStyle itemCheckTextStyle() {
    return TextStyle(
      fontSize: 13.0,
      fontFamily: "Averta",
      color: Theme.of(context).accentColor,
    );
  }

  TextStyle initDescriptionStyle() {
    return TextStyle(
      fontSize: 12,
      fontFamily: "Averta",
      color: Theme.of(context).highlightColor,
      fontWeight: FontWeight.w400,
    );
  }
}
