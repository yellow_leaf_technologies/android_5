import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/main.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/dialogs/choose_theme_dialog.dart';
import 'package:wevpn/widget/launchers/dialog_launchers.dart';
import 'package:wevpn/widget/text_field.dart';

class SettingVpnProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SettingVpnProfile();
  }
}

class _SettingVpnProfile extends State<SettingVpnProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: prefix0.Theme.of(context).backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 60, 0, 0),
            child: Align(
              child: Image.asset(
                MyApp.isLight
                    ? "assets/images/ic_logo.png"
                    : "assets/images/ic_logo_dark.png",
                width: MediaQuery.of(context).size.width / 3.2,
              ),
            ),
          ),
          Column(
            children: <Widget>[
              Image.asset(
                MyApp.isLight
                    ? "assets/images/ic_setting_vpn.png"
                    : "assets/images/ic_setting_vpn_dark.png",
                width: MediaQuery.of(context).size.width / 3,
              ),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 32, 0, 0),
                  child: CustomText(
                    "Set Up VPN Profile",
                    fontSize: 20.0,
                    fontWeight: FontWeight.w600,
                    textColor: AppColors.PINK,
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                  child: Text(
                    "Allow the app to create VPN profile\non your device to complete\nthe configuration",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 14.0,
                        height: 2.0,
                        color: prefix0.Theme.of(context).accentColor,
                        fontFamily: "Averta",
                        fontWeight: FontWeight.w400),
                  )),
            ],
          ),
          ButtonFullAction(
            "Continue Configuration",
            () async {
              _openChooseTheme();
            },
            marginLeft: 40,
            marginRight: 40,
            marginBottom: 60,
            height: AppSizes.CUSTOM_BUTTON_SIZE,
          ),
        ],
      ),
    );
  }

  void _openChooseTheme() async {
    await DialogLaunchers.showDialog(
        context: context,
        dialog: ChooseThemeDialog(

        ));
  }
}
