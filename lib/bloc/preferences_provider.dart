import 'package:shared_preferences/shared_preferences.dart';

class PreferencesProvider {
  static const KEY_FIRST_RUN = "first_run";
  static const THEME = "theme";
  static PreferencesProvider _instance;
  Future _initialized;
  SharedPreferences _preferences;

  factory PreferencesProvider() {
    if (_instance == null) {
      _instance = PreferencesProvider._();
    }
    return _instance;
  }

  PreferencesProvider._() {
    _initialized = _initPreferences();
  }

  Future _initPreferences() async {
    _preferences = await SharedPreferences.getInstance();
  }

  Future<bool> getFirstRunFinished() async {
    await _initialized;
    bool result = _preferences.getBool(KEY_FIRST_RUN);
    return result ?? false;
  }

  void saveFirstRunFinished() {
    _preferences.setBool(KEY_FIRST_RUN, true);
  }

  Future saveTheme(bool value) async {
    return _preferences.setBool(THEME, value);
  }

  Future getLightTheme() async {
    await _initialized;
    return _preferences.getBool(THEME) ?? true;
  }
}
