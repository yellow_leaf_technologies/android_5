import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/choose_location_screen.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/main.dart';
import 'package:wevpn/settings_screen.dart';
import 'package:wevpn/widget/raised_action.dart';
import 'package:wevpn/widget/text_field.dart';

class DashboardScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashboardScreen();
  }
}

class _DashboardScreen extends State<DashboardScreen> {
  bool isConnect = false;

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        home: Scaffold(
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(
                    MediaQuery.of(context).size.width < 400 ? 58.0 : 96.0),
                child: Container(
                    alignment: Alignment(-1.0, -1.0),
                    width: MediaQuery.of(context).size.width,
                    color: isConnect
                        ? AppColors.GREEN_WHITE
                        : Theme.of(context).hoverColor,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(20, 45, 0, 0),
                      child: new IconButton(
                        padding: EdgeInsets.all(5),
                        icon: Image.asset(
                          isConnect || !MyApp.isLight
                              ? "assets/images/ic_menu_white.png"
                              : "assets/images/ic_menu.png",
                          width: 22,
                        ),
                        onPressed: () => _openSettings(),
                      ),
                    ))),
            body: new Stack(
              children: [
                Container(
                    color: Theme.of(context).backgroundColor,
                    child: new Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        headerWidget(),
                        bodyWidget(),
                        bottomWidget(),
                      ],
                    )),
                Center(child: powerWidget())
              ],
            )));
  }

  void _openSettings() async {
    await Navigator.of(context)
        .push(MaterialPageRoute(builder: (context) => SettingsScreen()));
  }

  void _openChooseLocationScreen() async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ChooseLocationScreen()),
    );
  }

  Column bodyWidget() {
    double paddingLocation, paddingTop;
    if (MediaQuery.of(context).size.width < 400) {
      paddingLocation = 16.0;
      paddingTop = 62.0;
    } else {
      paddingTop = 72.0;
      paddingLocation = 26.0;
    }
    return Column(children: <Widget>[
      Padding(
        padding: EdgeInsets.fromLTRB(0, paddingTop, 0, 0),
        child: CustomText(
          "45 Server Locations",
          textStyle: TextStyle(
            fontSize: 13.0,
            fontFamily: "Averta",
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(AppSizes.LOCATION_RADIUS),
            ),
            color: Theme.of(context).hoverColor,
          ),
          margin: EdgeInsets.fromLTRB(40, 16, 40, 0),
          child: FlatButton(
            color: Theme.of(context).primaryColor,
              onPressed: () {
                _openChooseLocationScreen();
              },
              child: Column(children: <Widget>[
                Container(
                    padding: EdgeInsets.fromLTRB(
                        8, paddingLocation, 0, paddingLocation - 1),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        CustomText(
                          "Current Location:",
                          textColor: Theme.of(context).accentColor,
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
                          child: Row(children: <Widget>[
                            Image.asset(
                              "assets/images/ic_usa_flag_temp.png",
                              width: 26,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(12, 0, 0, 0),
                                child: CustomText(
                                  "USA - New York",
                                  fontSize: 18.0,
                                  textColor: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w600,
                                )),
//                            Padding(
//                                padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
//                                child: Image.asset(
//                                  "assets/images/ic_go.png",
//                                  width: 8.0,
//                                ))
                          ]),
                        )
                      ],
                    )),
              ])))
    ]);
  }

  Container powerWidget() {
    double paddingTop = 0;
    if (MediaQuery.of(context).size.width > 420) {
      paddingTop = 32;
    } else if (MediaQuery.of(context).size.width > 400) {
      paddingTop = 12;
    } else {
      paddingTop = 0;
    }
    return Container(
        child: Padding(
            padding: EdgeInsets.only(top: paddingTop),
            child: Column(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Container(
                      padding: EdgeInsets.fromLTRB(14, 7, 15, 9),
                      color: isConnect
                          ? AppColors.TRANSPARENT
                          : Theme.of(context).canvasColor,
                      child: isConnect
                          ? Text(
                              "CONNECTED!",
                              style: TextStyle(
                                  color: AppColors.WHITE,
                                  fontFamily: "Segoe",
                                  fontSize: 16),
                            )
                          : CustomText(
                              "Click to connect",
                                textColor: Theme.of(context).accentColor,
                            ),
                    )),
                isConnect
                    ? RaisedAction(
                        MyApp.isLight
                            ? "assets/images/ic_power.png"
                            : "assets/images/ic_power_dark.png", () {
                        setState(() {
                          isConnect = false;
                        });
                      })
                    : RaisedAction(
                        MyApp.isLight
                            ? "assets/images/ic_un_active_power.png"
                            : "assets/images/ic_un_active_power_dark.png", () {
                        setState(() {
                          isConnect = true;
                        });
                      }),
              ],
            )));
  }

  Widget bottomWidget() {
    double padding;
    if (MediaQuery.of(context).size.width < 400) {
      padding = 20.0;
    } else {
      padding = 50.0;
    }
    return new Container(
      height: MediaQuery.of(context).size.height / 4,
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40.0), topRight: Radius.circular(40.0)),
        color: Theme.of(context).hoverColor,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(40, 40, 40, 0),
                child: Column(
                  children: <Widget>[
                    CustomText(
                      "IP:",
                      textColor: Theme.of(context).cardColor,
                    ),
                    CustomText("192.168.547.15",
                        textColor: Theme.of(context).accentColor)
                  ],
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(
                    MediaQuery.of(context).size.width / 7, 40, 40, 0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    CustomText(
                      "VPN IP:",
                      textColor: Theme.of(context).cardColor,
                    ),
                    CustomText("145.581.234.54",
                        textColor: Theme.of(context).accentColor)
                  ],
                )),
          ]),
          Container(
            width: MediaQuery.of(context).size.width - 80,
            height: 1,
            color: AppColors.LINE_COLOR,
          ),
          Padding(
              padding: EdgeInsets.fromLTRB(40, 0, 40, padding),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.all(0),
                    child: Row(
                      children: <Widget>[
                        Image.asset("assets/images/ic_down.png", width: 13),
                        Padding(
                            padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
                            child: CustomText("0 kbps",
                                fontSize: 13,
                                textColor: Theme.of(context).accentColor))
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.all(0),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/images/ic_up.png", width: 13),
                          Padding(
                              padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
                              child: CustomText("0 kbps",
                                  fontSize: 13,
                                  textColor: Theme.of(context).accentColor))
                        ],
                      )),
                  Padding(
                      padding: EdgeInsets.all(0),
                      child: Row(
                        children: <Widget>[
                          Image.asset("assets/images/ic_time_clock.png",
                              width: 13),
                          Padding(
                              padding: EdgeInsets.fromLTRB(7, 0, 0, 0),
                              child: CustomText("00:00",
                                  fontSize: 13,
                                  textColor: Theme.of(context).accentColor))
                        ],
                      ))
                ],
              )),
        ],
      ),
    );
  }

  Widget headerWidget() {
    var header = new Container(
      height: MediaQuery.of(context).size.height / 5,
      width: MediaQuery.of(context).size.width,
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(40.0),
            bottomRight: Radius.circular(40.0)),
        color: isConnect ? AppColors.GREEN_WHITE : Theme.of(context).hoverColor,
      ),
    );
    return header;
  }
}
