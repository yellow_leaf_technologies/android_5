import 'package:flutter/material.dart';
import 'package:wevpn/auth/auth_screen.dart';
import 'package:wevpn/auth/existing_user_login.dart';
import 'package:wevpn/bloc/preferences_provider.dart';
import 'package:wevpn/main.dart';
import 'package:wevpn/onboarding_screen.dart';

class InitialScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _InitialScreenState();
  }
}

class _InitialScreenState extends State<InitialScreen>
    with TickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    super.initState();
    animationController = new AnimationController(
      vsync: this,
      duration: new Duration(seconds: 5),
    );
    animationController.forward();
    animationController.addListener(() {
      setState(() {
        if (animationController.status == AnimationStatus.completed) {
          animationController.repeat();
        }
      });
    });
    animationController.repeat();
    _initRedirect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 32),
            child: Align(
              child: Image.asset(
                MyApp.isLight
                    ? "assets/images/ic_logo.png"
                    : "assets/images/ic_logo_dark.png",
                width: MediaQuery.of(context).size.width / 2,
              ),
            ),
          ),
          new AnimatedBuilder(
            animation: animationController,
            child: new Container(
              height: 56.0,
              width: 56.0,
              child: new Image.asset('assets/images/ic_indicator.png'),
            ),
            builder: (BuildContext context, Widget _widget) {
              return new Transform.rotate(
                angle: animationController.value * 16,
                child: _widget,
              );
            },
          ),
        ],
      ),
    );
  }

  void _initRedirect() async {
    await Future.delayed(Duration(seconds: 3));
    _openScreen();
  }

  void _openScreen() async {
    PreferencesProvider provider = PreferencesProvider();
//    TODO first run open auth screen
    bool hasToShowOnboarding = await provider.getFirstRunFinished();
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => OnboardingScreen()),
    );
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }
}
