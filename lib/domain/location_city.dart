class LocationCity {
  int id;
  String name;

  LocationCity(this.id, this.name);

  getId() {
    return id;
  }

  getName() {
    return name;
  }
}
