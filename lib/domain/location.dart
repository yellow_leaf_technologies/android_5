import 'package:wevpn/domain/location_city.dart';

class Location {
  int id;
  String name;
  String flagPath;
  List<LocationCity> cityList;
  bool isFavorite;

  Location(this.id, this.name, this.flagPath, this.cityList, this.isFavorite);

  getId() {
    return id;
  }

  getName() {
    return name;
  }

  getFlagPath() {
    return flagPath;
  }

  getCityList() {
    return cityList;
  }

  setFavorite(bool isFavorite) {
    this.isFavorite = isFavorite;
  }

  getFavorite() {
    return isFavorite;
  }
}
