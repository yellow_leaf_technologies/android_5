import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/pages/all_location_page.dart';
import 'package:wevpn/pages/favorite_location_page.dart';
import 'package:wevpn/widget/search_widget.dart';

class ChooseLocationScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ChooseLocationScreen();
  }
}

class _ChooseLocationScreen extends State<ChooseLocationScreen>
    with SingleTickerProviderStateMixin {
  final GlobalKey tabsKey = GlobalKey();
  bool _keyboardIsShown = false;
  bool _searchIsActive = false;

  @override
  void initState() {
    super.initState();
  }

  void _onKeyboardAppeared(bool value) {
    setState(() {
      _keyboardIsShown = value;
    });
  }

  void _onSearchStarted() {
    setState(() {
      _searchIsActive = true;
    });
//    TODO current state is search result true
  }

  void _onSearchCancel() {
    setState(() {
      _searchIsActive = false;
    });
    //    TODO current state is search result false
  }

  Widget _allLocationPage() {
    return AllLocationPage();
  }

  Widget _favoriteLocationPage() {
    return FavoriteLocationPage();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(
                MediaQuery.of(context).size.width < 400 ? 200 : 212),
            child: Column(children: <Widget>[
              Padding(
                  padding: EdgeInsets.fromLTRB(20, 41, 0, 0),
                  child: Stack(children: <Widget>[
                    new IconButton(
                      padding: EdgeInsets.all(5),
                      icon: Image.asset(
                        "assets/images/ic_back.png",
                        width: 28.0,
                      ),
                      onPressed: () => Navigator.of(context).pop(null),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
                        child: Center(
                            child: Text(
                          "Choose a location",
                          style: TextStyle(
                              color: Theme.of(context).accentColor,
                              fontSize: 18.0,
                              fontFamily: "Averta",
                              fontWeight: FontWeight.w600),
                        )))
                  ])),
              PreferredSize(
                preferredSize: Size.fromHeight(150.0),
                child: Column(
                  children: <Widget>[
                    SearchWidget(
                      null,
                      onFocus: _onKeyboardAppeared,
                      onSearchStarted: _onSearchStarted,
                      onSearchCancel: _onSearchCancel,
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(50, 0, 50, 0),
                        child: TabBar(
                          labelStyle: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Averta"),
                          unselectedLabelStyle: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.w400,
                              fontFamily: "Averta"),
                          labelColor: AppColors.PINK,
                          unselectedLabelColor: Theme.of(context).accentColor,
                          indicatorColor: AppColors.PINK,
                          tabs: <Widget>[
                            Tab(
                              text: "All Locations",
                            ),
                            Tab(
                              text: "My Favorites (1)",
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ]),
          ),
          body: TabBarView(
            children: <Widget>[_allLocationPage(), _favoriteLocationPage()],
          ),
        ));
  }
}
