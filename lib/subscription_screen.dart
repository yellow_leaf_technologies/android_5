import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/auth/auth_screen.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/text_field.dart';

class SubscriptionScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SubscriptionScreenState();
  }
}

class _SubscriptionScreenState extends State<SubscriptionScreen> {
  bool _isYear = true;
  bool _isShowDetails = false;

  void _openAuthScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => AuthScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(40, 60, 40, 12),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Image.asset(
                      "assets/images/ic_logo.png",
                      width: 91,
                    ),
                    IconButton(
                      icon: Image.asset(
                        "assets/images/ic_cancel.png",
                        width: 26,
                      ),
                      onPressed: () {
                        _openAuthScreen();
                      },
                    )
                  ],
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(40, 0, 0, 0),
                child: CustomText("Select a plan",
                    fontWeight: FontWeight.w600,
                    fontSize: 20.0,
                    textColor: AppColors.PINK)),
            subscriptionWidget(),
            Container(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CustomText("Start your 7-day free trial.",
                        fontWeight: FontWeight.w600),
                    Padding(
                        padding: EdgeInsets.fromLTRB(0, 8, 0, 16),
                        child: CustomText("Then \$69.96 every 12 months",
                            textColor: AppColors.UNDERLINE_TEXT,
                            fontSize: 13.0)),
                  ],
                )),
            ButtonFullAction(
              "Buy a subscription",
              () {
//              TODO buy subscription
              },
              marginLeft: 28,
              marginRight: 28,
              height: AppSizes.CUSTOM_BUTTON_SIZE,
            ),
            subscribeDescriptionWidget(),
            Container(
                width: MediaQuery.of(context).size.width,
                child: FlatButton(
                    onPressed: () {
                      setState(() {
                        _isShowDetails = !_isShowDetails;
                      });
                    },
                    padding: EdgeInsets.all(5),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Subscription details",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontFamily: "Averta",
                              color: AppColors.PINK),
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(6, 0, 0, 0),
                            child: Image.asset(
                              _isShowDetails
                                  ? "assets/images/ic_up_detail.png"
                                  : "assets/images/ic_down_detail.png",
                              width: 8,
                            )),
                      ],
                    ))),
            _isShowDetails ? showDetailWidget() : Container()
          ],
        )));
  }

  Widget showDetailWidget() {
    return Container(
      padding: EdgeInsets.fromLTRB(28.0, 0, 28.0, 26.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          CustomText(
            "• Payment will be charged to your iTunes account\nat confirmation of purchase.\n\n"
            "• Subscription automatically renews\nunless renew is turned off at least 24hrs before the\nend of the current period\n\n"
            "• Account will be charged for renewal within 24hrs\nprior to the end of the current period, and identify\nthe cost of the renewal.\n\n"
            "• Any unused portion of a free trial period, if offered, will be forfeited when the user purchases a subscription to that publication, where applicable\n\n"
            "• By subscribing your agree to our",
            textColor: Theme.of(context).accentColor,
            fontSize: 13.0,
          ),
          Row(
            children: <Widget>[
              FlatButton(
                  onPressed: () {
                    setState(() {});
                  },
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "Terms of Service",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: "Averta",
                        color: AppColors.PINK),
                  )),
              CustomText(
                "and",
                textColor: Theme.of(context).accentColor,
                fontSize: 13.0,
              ),
              FlatButton(
                  onPressed: () {
                    setState(() {});
                  },
                  padding: EdgeInsets.all(5),
                  child: Text(
                    "Privacy Policy",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 13.0,
                        fontFamily: "Averta",
                        color: AppColors.PINK),
                  ))
            ],
          )
        ],
      ),
    );
  }

  Widget subscriptionWidget() {
    return Container(
        decoration: BoxDecoration(
          color: AppColors.WHITE,
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        margin: EdgeInsets.fromLTRB(28.0, 20.0, 28.0, 16.0),
        child: Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 21.0),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Theme(
                      data: ThemeData(
                        unselectedWidgetColor: AppColors.CHECK_BOX_UNCHECK,
                        accentColor: AppColors.PINK,
                      ),
                      child: RadioListTile(
                        title: CustomText(
                          "Yearly",
                          fontWeight: FontWeight.w600,
                          fontSize: 16.0,
                        ),
                        groupValue: _isYear,
                        value: true,
                        onChanged: (value) {
                          setState(() {
                            _isYear = value;
                          });
                        },
                      )),
                  CustomText("\$69.96 Billed every 12 months",
                      textColor: AppColors.TEXT_DESCRIPTION, fontSize: 13.0),
                  Container(
                    margin: EdgeInsets.fromLTRB(24, 16, 24, 0),
                    color: AppColors.LINE_SUB_COLOR,
                    width: MediaQuery.of(context).size.width,
                    height: 1,
                  ),
                  Theme(
                      data: ThemeData(
                        unselectedWidgetColor: AppColors.CHECK_BOX_UNCHECK,
                        accentColor: AppColors.PINK,
                      ),
                      child: RadioListTile(
                        title: CustomText(
                          "Monthly",
                          fontWeight: FontWeight.w600,
                          fontSize: 16.0,
                        ),
                        groupValue: _isYear,
                        value: false,
                        onChanged: (value) {
                          setState(() {
                            _isYear = value;
                          });
                        },
                      )),
                  CustomText("\$10.99 Billed every month",
                      textColor: AppColors.TEXT_DESCRIPTION, fontSize: 13.0),
                ])));
  }

  Widget subscribeDescriptionWidget() {
    return Container(
        margin: EdgeInsets.fromLTRB(28, 20, 0, 32),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CustomText("Why subscribe?",
                  fontSize: 16.0, fontWeight: FontWeight.w600),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                  child: Row(
                    children: <Widget>[
                      Image.asset("assets/images/ic_devices.png", width: 24.0),
                      Padding(
                          padding: EdgeInsets.fromLTRB(9, 0, 0, 0),
                          child: CustomText("Protect for all devices")),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(33, 0, 0, 0),
                  child: CustomText("10 connections under a single account",
                      fontSize: 13.0, textColor: AppColors.TEXT_DESCRIPTION)),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                  child: Row(
                    children: <Widget>[
                      Image.asset("assets/images/ic_incognito.png",
                          width: 24.0),
                      Padding(
                          padding: EdgeInsets.fromLTRB(9, 0, 0, 0),
                          child: CustomText("Privacy anywhere, anytime")),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(33, 0, 0, 0),
                  child: CustomText("Hundreds of VPN servers worldwide",
                      fontSize: 13.0, textColor: AppColors.TEXT_DESCRIPTION)),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                  child: Row(
                    children: <Widget>[
                      Image.asset("assets/images/ic_pc_play.png", width: 24.0),
                      Padding(
                          padding: EdgeInsets.fromLTRB(9, 0, 0, 0),
                          child: CustomText(
                              "Safe access to the content you love")),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(33, 0, 0, 0),
                  child: CustomText("Unlimited browsing, streaming & download",
                      fontSize: 13.0, textColor: AppColors.TEXT_DESCRIPTION)),
              Padding(
                  padding: EdgeInsets.fromLTRB(0, 18, 0, 0),
                  child: Row(
                    children: <Widget>[
                      Image.asset("assets/images/ic_content.png", width: 18.0),
                      Padding(
                          padding: EdgeInsets.fromLTRB(13, 0, 0, 0),
                          child: CustomText("Strict No-Logs policy")),
                    ],
                  )),
              Padding(
                  padding: EdgeInsets.fromLTRB(37, 0, 0, 0),
                  child: CustomText(
                      "Absolutely no logs of your internet traffic",
                      fontSize: 13.0,
                      textColor: AppColors.TEXT_DESCRIPTION)),
            ]));
  }
}
