import 'package:flip_card/flip_card.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/auth/auth_screen.dart';
import 'package:wevpn/auth/existing_user_login.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/text_field.dart';

class OnboardingScreen extends StatefulWidget {
  const OnboardingScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _OnboardingScreenState();
  }
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  static const _TOP_PROPORTION = 4;
  static const _PAGES_LENGTH = 4;

  final PageController controller = new PageController();

  Map<int, GlobalKey<FlipCardState>> cardKeys = {};

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < _PAGES_LENGTH; i++) {
      GlobalKey<FlipCardState> key = GlobalKey<FlipCardState>();
      setState(() {
        cardKeys[i] = key;
      });
    }

    Future.delayed(Duration(milliseconds: 500), () {
      cardKeys[0].currentState.toggleCard();
    });
  }

  void _openExistingUserLoginScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => ExistingUserLogin()),
    );
  }

  void _openAuthScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => AuthScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> pages = <Widget>[
      _firstPage,
      _secondPage,
      _thirdPage,
      _fourthPage
    ];
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: 135,
            // color: AppColors.BACKGROUND,
            // padding: EdgeInsets.only(top: 16, bottom: 0),
            child: PageView(
              physics: BouncingScrollPhysics(),
              children: pages,
              controller: controller,
              onPageChanged: _onPageChanged,
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 135,
            child: Padding(
              padding: EdgeInsets.only(top: 13.0, bottom: 24.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  for (var i = 0; i < _PAGES_LENGTH; i++) getCircleBar(i)
                ],
              ),
            ),
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Padding(
              padding: EdgeInsets.only(top: 16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ButtonFullAction(
                    "Start your 7-day FREE trial",
                    () {
                      _openExistingUserLoginScreen();
                    },
                    marginLeft: 40,
                    marginRight: 40,
                    height: AppSizes.CUSTOM_BUTTON_SIZE,
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0, 6, 0, 30),
                    child: FlatButton(
                        onPressed: () {
                          setState(() {
                            _openAuthScreen();
                          });
                        },
                        padding: EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CustomText(
                              "Existing User? Login",
                              fontSize: 16,
                              textColor: AppColors.PINK,
                              fontWeight: FontWeight.w600,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(6, 0, 0, 0),
                                child: Image.asset(
                                  "assets/images/ic_sub.png",
                                  width: 8,
                                )),
                          ],
                        )),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onPageChanged(int page) {
    for (var i = 0; i < _PAGES_LENGTH; i++) {
      if (cardKeys[i].currentState != null &&
          !cardKeys[i].currentState.isFront) {
        cardKeys[i].currentState.toggleCard();
      }
    }

    final cardKey = cardKeys[page];
    cardKey.currentState.toggleCard();
  }

  Widget getCircleBar(int i) {
    return FlipCard(
      key: cardKeys[i],
      flipOnTouch: false,
      speed: 300,
      direction: FlipDirection.HORIZONTAL,
      front: Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        height: 8,
        width: 8,
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
      ),
      back: Container(
        margin: EdgeInsets.symmetric(horizontal: 5),
        height: 8,
        width: 8,
        decoration: BoxDecoration(
          color: AppColors.PINK,
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
      ),
    );
  }

  Widget get _firstPage => Padding(
      padding: EdgeInsets.fromLTRB(24.0, 53.0, 24.0, 48.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: _TOP_PROPORTION,
              child: Center(
                child: Image.asset(
                  "assets/images/ic_onboarding_first.png",
                  filterQuality: FilterQuality.medium,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: CustomText(
                  "7-day free trial",
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                )),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: CustomText(
                "If you don’t love it, Cancel anytime",
                textColor: Theme.of(context).unselectedWidgetColor,
              ),
            ),
          ]));

  Widget get _secondPage => Padding(
      padding: EdgeInsets.fromLTRB(24.0, 53.0, 24.0, 48.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: _TOP_PROPORTION,
              child: Center(
                child: Image.asset(
                  "assets/images/ic_onboarding_second.png",
                  filterQuality: FilterQuality.medium,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: CustomText(
                  "458+ servers in 24+ countries",
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                )),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: CustomText(
                "Pick, Connect and enjoy the best internet",
                textColor: AppColors.UNDERLINE_TEXT,
              ),
            ),
          ]));

  Widget get _thirdPage => Padding(
      padding: EdgeInsets.fromLTRB(24.0, 53.0, 24.0, 48.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: _TOP_PROPORTION,
              child: Center(
                child: Image.asset(
                  "assets/images/ic_onboarding_third.png",
                  filterQuality: FilterQuality.medium,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: CustomText(
                  "Strict No-Logs policy",
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                )),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: CustomText(
                "We take data security to another level",
                textColor: AppColors.UNDERLINE_TEXT,
              ),
            ),
          ]));

  Widget get _fourthPage => Padding(
      padding: EdgeInsets.fromLTRB(24.0, 53.0, 24.0, 48.0),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Expanded(
              flex: _TOP_PROPORTION,
              child: Center(
                child: Image.asset(
                  "assets/images/ic_onboarding_fourth.png",
                  filterQuality: FilterQuality.medium,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 16, 0, 0),
                child: CustomText(
                  "Secure your digital life",
                  fontWeight: FontWeight.w600,
                  fontSize: 16.0,
                )),
            Padding(
              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
              child: CustomText(
                "With easy-to-manage, Powerful features",
                textColor: AppColors.UNDERLINE_TEXT,
              ),
            ),
          ]));
}
