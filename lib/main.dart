import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wevpn/bloc/preferences_provider.dart';
import 'package:wevpn/initial_screen.dart';
import 'package:wevpn/themes/custom_theme.dart';
import 'package:wevpn/themes/themes.dart';

void main() async {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    PreferencesProvider provider = PreferencesProvider();
    bool isLight = await provider.getLightTheme();
    MyApp.isLight = isLight;
    runApp(CustomTheme(
        initialThemeKey: isLight ? ThemeKeys.LIGHT : ThemeKeys.DARK,
        child: MyApp()));
  });
}

class MyApp extends StatelessWidget {
  static bool isLight = true;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'WeVPN', theme: CustomTheme.of(context), home: InitialScreen());
  }
}
