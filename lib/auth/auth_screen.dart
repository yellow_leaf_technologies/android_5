import 'package:flutter/material.dart';
import 'package:wevpn/bloc/preferences_provider.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/main.dart';
import 'package:wevpn/setting_vpn_profile_screen.dart';
import 'package:wevpn/subscription_screen.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/dialogs/dialog_invalid_login.dart';
import 'package:wevpn/widget/launchers/dialog_launchers.dart';
import 'package:wevpn/widget/login_form.dart';
import 'package:wevpn/widget/text_field.dart';

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthScreen();
}

class _AuthScreen extends State<AuthScreen> {
  void _openSubscriptionScreen() async {
    await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SubscriptionScreen()),
    );
  }

  @override
  void initState() {
    super.initState();
    PreferencesProvider().saveFirstRunFinished();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 57.25),
            child: Align(
              child: Image.asset(
                MyApp.isLight
                    ? "assets/images/ic_logo.png"
                    : "assets/images/ic_logo_dark.png",
                width: MediaQuery.of(context).size.width / 2,
              ),
            ),
          ),
          LoginForm(),
          ButtonFullAction(
            "Login to your account",
            () async {
              _invalidLogin();
            },
            marginTop: 20,
            marginLeft: 40,
            marginRight: 40,
            height: AppSizes.CUSTOM_BUTTON_SIZE,
          ),
          Padding(
              padding: EdgeInsets.fromLTRB(0, 18, 0, 5),
              child: FlatButton(
                  onPressed: () {
                    setState(() {
                      _openSubscriptionScreen();
                    });
                  },
                  padding: EdgeInsets.all(5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      CustomText(
                        "Buy a new subscription",
                        fontSize: 16,
                        textColor: AppColors.PINK,
                        fontWeight: FontWeight.w600,
                      ),
                      Padding(
                          padding: EdgeInsets.fromLTRB(6, 0, 0, 0),
                          child: Image.asset(
                            "assets/images/ic_sub.png",
                            width: 8,
                          )),
                    ],
                  ))),
        ],
      ),
    );
  }

  void _invalidLogin() async {
    await DialogLaunchers.showDialog(
        context: context,
        dialog: InvalidateLoginDialog(
          yesAction: () {
            _openSettingProfileScreen();
          },
          noAction: () {},
        ));
  }

  void _openSettingProfileScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => SettingVpnProfile()),
    );
  }
}
