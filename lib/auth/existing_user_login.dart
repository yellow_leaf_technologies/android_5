import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/auth/auth_screen.dart';
import 'package:wevpn/constanst/app_colors.dart';
import 'package:wevpn/constanst/app_sizes.dart';
import 'package:wevpn/widget/button_full_action.dart';
import 'package:wevpn/widget/login_form.dart';
import 'package:wevpn/widget/text_field.dart';

class ExistingUserLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ExistingUserLogin();
}

class _ExistingUserLogin extends State<ExistingUserLogin> {
  void _openAuthScreen() async {
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(builder: (context) => AuthScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        body: SingleChildScrollView(
            padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.fromLTRB(40, 60, 40, 38),
                    color: Theme.of(context).hoverColor,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Image.asset(
                              "assets/images/ic_logo.png",
                              width: 91,
                            ),
                            IconButton(
                              icon: Image.asset(
                                "assets/images/ic_cancel.png",
                                width: 26,
                              ),
                              onPressed: () {
                                _openAuthScreen();
                              },
                            )
                          ],
                        ),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 56, 0, 0),
                            child: CustomText("Let’s get you started",
                                fontWeight: FontWeight.w600,
                                fontSize: 20.0,
                                textColor: AppColors.PINK)),
                        Padding(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: CustomText(
                                "You’re just a few steps away from\nyour praivate and secure internet life.",
                                textColor: Theme.of(context).accentColor))
                      ],
                    ),
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
                      child: LoginForm()),
                  ButtonFullAction(
                    "Login to your account",
                    () async {
                      _openAuthScreen();
                    },
                    marginTop: 20,
                    marginLeft: 40,
                    marginRight: 40,
                    height: AppSizes.CUSTOM_BUTTON_SIZE,
                  ),
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 12, 0, 0),
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            CustomText(
                              "Existing User? Login",
                              fontSize: 16,
                              textColor: AppColors.PINK,
                              fontWeight: FontWeight.w600,
                            ),
                            Padding(
                                padding: EdgeInsets.fromLTRB(6, 0, 0, 0),
                                child: Image.asset(
                                  "assets/images/ic_sub.png",
                                  width: 8,
                                ))
                          ])),
                  Padding(
                      padding: EdgeInsets.fromLTRB(0, 21, 0, 0),
                      child: CustomText(
                        "By creating an account you agree to our",
                        textColor: Theme.of(context).accentColor,
                        fontSize: 13.0,
                      )),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      FlatButton(
                          onPressed: () {
                            setState(() {});
                          },
                          padding: EdgeInsets.all(5),
                          child: Text(
                            "Terms of Service",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 13.0,
                                fontFamily: "Averta",
                                color: AppColors.PINK),
                          )),
                      CustomText(
                        "and",
                        textColor: Theme.of(context).accentColor,
                        fontSize: 13.0,
                      ),
                      FlatButton(
                          onPressed: () {
                            setState(() {});
                          },
                          padding: EdgeInsets.all(5),
                          child: Text(
                            "Privacy Policy",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 13.0,
                                fontFamily: "Averta",
                                color: AppColors.PINK),
                          ))
                    ],
                  )
                ])));
  }
}
