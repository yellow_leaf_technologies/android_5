import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wevpn/domain/location.dart';
import 'package:wevpn/domain/location_city.dart';
import 'package:wevpn/widget/text_field.dart';

class AllLocationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AllLocationPage();
  }
}

class _AllLocationPage extends State<AllLocationPage> {
//  Location(this.id, this.name, this.flagPath, this.cityList, this.isFavorite);

//TODO temp location list
  final controller = StreamController<List<Location>>();

  Stream<List<Location>> getTempLocationList() {
    List<Location> location = new List();
    List<LocationCity> unitSates = new List();
    unitSates.add(LocationCity(1, "United States 1"));
    unitSates.add(LocationCity(2, "United States 2"));
    location.add(Location(1, "United States",
        "assets/images/ic_usa_flag_temp.png", unitSates, false));
    List<LocationCity> canada = new List();
    canada.add(LocationCity(1, "CA Vancouver"));
    canada.add(LocationCity(2, "CA Toronto"));
    canada.add(LocationCity(3, "CA Montreal"));
    location.add(Location(
        2, "Canada", "assets/images/ic_temp_canada.png", canada, true));
    location.add(
        Location(3, "Mexico", "assets/images/ic_temp_mexico.png", null, false));
    location.add(Location(
        4, "Germany", "assets/images/ic_temp_germany.png", null, false));
    location.add(Location(
        5, "Austria", "assets/images/ic_temp_austria.png", null, false));
    location.add(Location(
        6, "Belgium", "assets/images/ic_temp_belgium.png", null, false));
    List<LocationCity> nethrlands = new List();
    nethrlands.add(LocationCity(1, "Nethrlands 1"));
    nethrlands.add(LocationCity(2, "Nethrlands 2"));
    nethrlands.add(LocationCity(3, "Nethrlands 3"));
    nethrlands.add(LocationCity(4, "Nethrlands 4"));
    location.add(Location(7, "Nethrlands",
        "assets/images/ic_temp_netherlands.png", nethrlands, false));
    location.add(Location(
        8, "Denmark", "assets/images/ic_temp_denmark.png", null, false));
    controller.add(location);
    return controller.stream;
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
            color: Theme.of(context).backgroundColor,
            child: StreamBuilder<List<Location>>(
              stream: getTempLocationList(),
              initialData: List(),
              builder:
                  (BuildContext context, AsyncSnapshot<List<Location>> data) {
                List<Location> list = data.data;
                if (list.length == 0) {
                  return Container();
                }
                return ListView.builder(
                    itemCount: list.length,
                    itemBuilder: (context, index) {
                      return ExpansionTile(
                        title: _buildParentExpandableContent(list[index]),
                        children: _buildExpandableContent(list[index]),
                      );
                    });
              },
            )));
  }

  _buildParentExpandableContent(Location location) {
    return Container(
      padding: EdgeInsets.fromLTRB(20, 0, 20, 0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Image.asset(location.getFlagPath(), width: 24),
                Padding(
                    padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                    child: CustomText(
                      location.getName(),
                      textColor: Theme.of(context).accentColor,
                      fontWeight: location.getFavorite()
                          ? FontWeight.w600
                          : FontWeight.w400,
                    )),
              ],
            ),
          ),
          IconButton(
            icon: Image.asset(
              location.getFavorite()
                  ? "assets/images/ic_favorite_enable.png"
                  : "assets/images/ic_favorite.png",
              width: 15,
            ),
            onPressed: () {},
          )
        ],
      ),
    );
  }

  _buildExpandableContent(Location location) {
    if (location.getCityList() == null || location.getCityList().length == 0) {
      return new List<Widget>();
    }
    List<Widget> columnContent = [];
    for (LocationCity city in location.getCityList())
      columnContent.add(
        new ListTile(
            title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.fromLTRB(54, 0, 0, 0),
                child: CustomText(
                  city.getName(),
                  textColor: Theme.of(context).accentColor,
                )),
            Padding(
                padding: EdgeInsets.fromLTRB(0, 0, 60, 0),
                child: IconButton(
                  icon: Image.asset(
                    location.getFavorite()
                        ? "assets/images/ic_favorite_enable.png"
                        : "assets/images/ic_favorite.png",
                    width: 15,
                  ),
                  onPressed: () {},
                ))
          ],
        )),
      );

    return columnContent;
  }
}
