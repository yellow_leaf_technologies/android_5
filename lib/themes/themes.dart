import 'package:flutter/material.dart';
import 'package:wevpn/constanst/app_colors.dart';

enum ThemeKeys { LIGHT, DARK }

class Themes {
  static final ThemeData lightTheme = ThemeData(
      brightness: Brightness.light,
      backgroundColor: AppColors.BACKGROUND,
      appBarTheme: AppBarTheme(
          elevation: 0,
          color: AppColors.BACKGROUND_BASE,
          textTheme: TextTheme(
              title: TextStyle(
                  color: AppColors.BACKGROUND_APP_BAR_TITLE,
                  fontSize: 18.0,
                  fontFamily: "Averta",
                  fontWeight: FontWeight.w600))),
      tabBarTheme: TabBarTheme(
          unselectedLabelStyle: TextStyle(
            color: AppColors.BACKGROUND_APP_BAR_TITLE,
            fontFamily: "Averta",
            fontSize: 14.0,
          ),
          labelStyle: TextStyle(
            color: AppColors.PINK,
            fontWeight: FontWeight.w600,
            fontFamily: "Averta",
            fontSize: 14.0,
          )),
      buttonTheme: ButtonThemeData(
        buttonColor: AppColors.PINK,
      ),
      hintColor: AppColors.BACKGROUND_TEXT_FIELD,
      accentColor: AppColors.BACKGROUND_APP_BAR_TITLE,
      unselectedWidgetColor: AppColors.UNDERLINE_TEXT,
      hoverColor: AppColors.BACKGROUND_BASE,
      cardColor: AppColors.SETTING_BLOCK_TITLE,
      highlightColor: AppColors.SETTING_DESCRIPTIONS,
      canvasColor: AppColors.WHITE,
      dividerColor: AppColors.BACKGROUND_LINE,
  primaryColor:AppColors.WHITE );

  static final ThemeData darkTheme = ThemeData(
      brightness: Brightness.dark,
      backgroundColor: AppColors.BACKGROUND_DART,
      appBarTheme: AppBarTheme(
          elevation: 0,
          color: AppColors.BACKGROUND_BASE_DART,
          textTheme: TextTheme(
              title: TextStyle(
                  color: AppColors.BACKGROUND_APP_BAR_TITLE_DART,
                  fontSize: 18.0,
                  fontFamily: "Averta",
                  fontWeight: FontWeight.w600))),
      tabBarTheme: TabBarTheme(
          unselectedLabelStyle: TextStyle(
            color: AppColors.BACKGROUND_APP_BAR_TITLE_DART,
            fontFamily: "Averta",
            fontSize: 14.0,
          ),
          labelStyle: TextStyle(
            color: AppColors.PINK,
            fontWeight: FontWeight.w600,
            fontFamily: "Averta",
            fontSize: 14.0,
          )),
      buttonTheme: ButtonThemeData(
        buttonColor: AppColors.PINK,
      ),
      hintColor: AppColors.BACKGROUND_TEXT_FIELD_DART,
      accentColor: AppColors.BACKGROUND_APP_BAR_TITLE_DART,
      unselectedWidgetColor: AppColors.UNDERLINE_TEXT_DART,
      hoverColor: AppColors.BACKGROUND_BASE_DART,
      cardColor: AppColors.SETTING_BLOCK_TITLE_DART,
      highlightColor: AppColors.SETTING_DESCRIPTIONS_DART,
      canvasColor: AppColors.BACKGROUND_TOAST_DART,
      dividerColor: AppColors.BACKGROUND_LINE_DART,
      primaryColor:AppColors.BACKGROUND_BASE_DART);

  static ThemeData getThemeFromKey(ThemeKeys themeKey) {
    switch (themeKey) {
      case ThemeKeys.LIGHT:
        return lightTheme;
      case ThemeKeys.DARK:
        return darkTheme;
      default:
        return lightTheme;
    }
  }
}
